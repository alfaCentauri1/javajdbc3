package test;

import datos.Conexion;
import datos.PersonaDAO;
import domain.Persona;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class TestManejoPersonas {
    public static void main(String[] args) {
        Connection conexion = null;
        try {
            conexion = Conexion.getConnection();
            if (conexion.getAutoCommit()) {
                conexion.setAutoCommit(false);
            }
            PersonaDAO personaDAO = new PersonaDAO(conexion);
            Persona nuevaPersona = new Persona("Alejandra", "Gomez", "agomez@dominio.com", 54123456789L);
            Persona nuevaPersona2 = new Persona("María", "Gomez", "agomez@dominio.com", 54123456789L);
            Persona nuevaPersona3 = new Persona("Julia", "Gomez123456789789456123369852147410238853695555555", "agomez@dominio.com", 54123456789L);
            personaDAO.insertar(nuevaPersona);
            personaDAO.insertar(nuevaPersona2);
            personaDAO.insertar(nuevaPersona3);
            List<Persona> listado = personaDAO.listar();
            for (Persona persona : listado) {
                System.out.println(persona);
            }
            Persona encontrada = (Persona) personaDAO.buscarPorId(2);
            if (encontrada != null) {
                personaDAO.eliminar(2);
                System.out.println("Fue borrado el registro de la persona: " + encontrada);
            }
            personaDAO.closeConection();
        }catch (SQLException exception){
            exception.printStackTrace(System.out);
            System.out.println("Entramos en un rollback");
            try {
                conexion.rollback();
            } catch (SQLException e) {
                System.out.println("Fallo el rollback: " + e.getMessage());
            }
        }
    }
}
